## Integrate Library Management System
- This project was created with help from Wes Doyle's Youtube Videos. The series can be found here: https://www.youtube.com/watch?v=WTVcLFTgDqs&list=PL3_YUnRN3Uhgi7llyOkCggxeY9cvW0N3K

## What was learnt:
- Create and update database from Visual Studio.
- How to correctly use the MVC pattern.
- Program was created using ASP .NET
- Create dynamic pages.
- Create validation for client and server side.
- OOP practices.